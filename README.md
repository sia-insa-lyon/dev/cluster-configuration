# SIA Kubernetes Cluster configuration

This repository contains files needed to configure any Kubernetes cluster in a suitable setup for the SIA.

The setup can be random YAML file definitions or application of Helm templates.

*Do not forget to have a look to the wiki of this repository to check for documentation.*

This folder defines the minimal services required to make our wonderful 
kubernetes a good place to deploy our applications.

Terraform handle the process to call the different helm command and get
your cluster up. Do not forget to store your state somewhere. The terraform state is currently stored using [Terraform Remote backend](https://www.terraform.io/docs/backends/types/remote.html).

## What are we deploying?

### Ingress Management
See [./ingress](./ingress)
#### HAProxy
#### External-DNS
#### Cert-Manager
### Object Storage
See [./minio](./minio)
#### Minio
### Database management
See [./kubedb](./kubedb)
#### KubeDB
See [Official KubeDB website](https://kubedb.com)
### Authentication Management
See [./keycloak](./keycloak)
#### Keycloak

## Note for Administrators

WARNING: Please read carefully what Terraform plan to do to the apply to not cause any downtime (or even dataloses)

All production work is done in the `production` terraform workspace.

