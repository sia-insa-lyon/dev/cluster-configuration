# Certificate Generator

This Terraform module handles generation of private SSL certificates.

For that it intend to use intermediates of a local Certification Authority
stored into `~/.tls`

## How to use it

```hcl-terraform
module "certificate_helm" {
  source = "../../certificate"
  OU = "Conseil de la Vie Associative"
  O = "BDE INSA Lyon"
  CN = "cva@bde-insa-lyon.fr"
  domains = []
  usages = [
    "key_encipherment",
    "digital_signature",
    "client_auth",
    "server_auth"
  ]
}
```
