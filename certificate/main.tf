variable ca_key {
  default = "~/.sia-admin/sensitive/intermediate_ca-key.pem"
}

variable ca_cert {
  default = "~/.sia-admin/sensitive/intermediate_ca.pem"
}

variable domains {
  type = "list"
}
variable CN {}
variable O {}
variable "OU" {
  default = ""
}
variable usages {
  default = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
  type = "list"
}

output certificate_key {
  value = "${tls_private_key.key.private_key_pem}"
}

output certificate_cert {
  value = "${tls_locally_signed_cert.signature.cert_pem}"
}

output ca_cert {
  value = "${file(var.ca_cert)}"
}

resource "tls_private_key" key {
  algorithm = "RSA"
  rsa_bits = "2048"
}
resource "tls_cert_request" request {
  key_algorithm = "${tls_private_key.key.algorithm}"
  private_key_pem = "${tls_private_key.key.private_key_pem}"
  dns_names = [
    "${var.domains}"]
  "subject" {
    common_name = "${var.CN}"
    organization = "${var.O}"
    organizational_unit = "${var.OU}"
  }
}

resource "tls_locally_signed_cert" signature {
  allowed_uses = [ "${var.usages}" ]
  ca_cert_pem = "${file(var.ca_cert)}"
  ca_private_key_pem = "${file(var.ca_key)}"
  cert_request_pem = "${tls_cert_request.request.cert_request_pem}"
  validity_period_hours = 8790
  early_renewal_hours = 7884
  ca_key_algorithm = "RSA"
}
