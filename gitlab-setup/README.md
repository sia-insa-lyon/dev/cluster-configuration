# GitLab for Kubernetes

GitLab has a native integration with Kubernetes but you will need to do a little
setup to get the information to give to GitLab.

## How to do
1. Configure your kubectl command to point to the k8s cluster
2. Run:
    ```
    bash ./run.sh the-namespce-to-give-access
    ```
3. Go to: https://gitlab.com/groups/sia-insa-lyon/-/clusters/new

## It does not work properly
The script used is extractly from the following documentation:

https://gitlab.com/help/user/project/clusters/index.md#adding-an-existing-kubernetes-cluster
