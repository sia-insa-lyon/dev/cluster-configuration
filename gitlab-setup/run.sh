#!/usr/bin/env bash

K8S_URL=`kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`
K8S_CA=
K8S_TOKEN=
NAMESPACE=${1:-default}

kubectl apply -n ${NAMESPACE} -f gitlab.yml

K8S_TOKEN_SECRET=`kubectl get -n ${NAMESPACE} serviceaccounts/gitlab -o jsonpath="{['secrets'][0]['name']}"`
echo
echo
echo "======================================================================"
echo "==  Configuration for GitLab"
echo "======================================================================"
echo "==== URL ===="
echo $K8S_URL
echo "==== CA ===="
kubectl get -n ${NAMESPACE} secret $K8S_TOKEN_SECRET -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
echo "==== TOKEN ===="
echo $K8S_TOKEN
echo "======================================================================"
