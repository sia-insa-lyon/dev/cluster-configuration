# Installing Helm / Tiller 

Helm requires Tiller to run on the cluster. However, by default, Tiller is 
insecured. Running just `helm init` is highly discouraged.

The `install-tiller.sh` will setup you a secured version of tiller. You pass
to it your Host and your current CA certificate and key.

The installation script will:
- Generate the server side certificate
- Create a Role Based Access Control (RBAC) in the cluster to authorize Tiller
- Run the `helm init` with the TLS settings

## TLDR:
```
./install-tiller.sh helm.kube.asso-insa-lyon.fr ~/.tls/intermediate_ca.pem ~/.tls/intermediate_ca-key.pem
```
