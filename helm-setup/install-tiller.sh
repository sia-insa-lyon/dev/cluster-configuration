#!/usr/bin/env bash

echo "${CA_PEM}" > ca.pem
echo "${SERVER_KEY}" > server-key.pem
echo "${SERVER_CRT}" > server.pem
helm init --upgrade --service-account tiller \
    --override 'spec.template.spec.containers[0].command'='{/tiller,--storage=secret}' \
    --tiller-tls \
    --tiller-tls-cert ./server.pem \
    --tiller-tls-key ./server-key.pem \
    --tiller-tls-verify \
    --tls-ca-cert ./ca.pem
rm ca.pem server-key.pem server.pem

