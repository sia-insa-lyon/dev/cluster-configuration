terraform {
  backend "remote" {
    organization = "sia-insa-lyon"
    "workspaces" {
      prefix = "helm-"
    }
  }
}

resource "tls_private_key" "helm" {
  algorithm   = "RSA"
  rsa_bits = 2048
}

resource "tls_private_key" "ca" {
  algorithm   = "RSA"
  rsa_bits = 2048
}

variable organization {
  type = "string"
  default = "BDE INSA Lyon"
}
variable organizational_unit {
  type = "string"
  default = "Conseil de la Vie Associative"
}
variable country {
  type = "string"
  default = "FR"
}

resource "tls_self_signed_cert" "ca" {
  key_algorithm   = "RSA"
  private_key_pem = "${tls_private_key.ca.private_key_pem}"

  subject {
    common_name  = "Helm CA"
    organization = "${var.organization}"
    organizational_unit = "${var.organizational_unit}"
    country = "${var.country}"
  }

  validity_period_hours = 219000 # This is 25 years

  is_ca_certificate = true

  allowed_uses = ["cert_signing"]
}

resource "tls_cert_request" "helm" {
  key_algorithm   = "RSA"
  private_key_pem = "${tls_private_key.helm.private_key_pem}"

  subject {
    common_name  = "helm"
    organization = "${var.organization}"
    organizational_unit = "${var.organizational_unit}"
    country = "${var.country}"
  }
  dns_names = [
    "tiller",
    # This match to the service used by tiller in the cluster
    "tiller.kube-system.cluster.local",
    # Localhost validation is need because HELM communicate on localhost
    "localhost",
    "127.0.0.1"
  ]
  ip_addresses = [
    "127.0.0.1"
  ]
}

resource "tls_locally_signed_cert" "helm" {
  cert_request_pem   = "${tls_cert_request.helm.cert_request_pem}"
  ca_key_algorithm   = "RSA"
  ca_private_key_pem = "${tls_private_key.ca.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca.cert_pem}"

  validity_period_hours = 219000 # This is 25 years

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth"
  ]
}

resource "null_resource" "kubernetes-roles" {
  provisioner "local-exec" {
    command = "kubectl apply -f ${pathexpand("${path.module}/helm-rbac.yaml")}"
  }
  provisioner "local-exec" {
    command = "kubectl delete --ignore-not-found -f ${pathexpand("${path.module}/helm-rbac.yaml")}"
    when = "destroy"
  }

}

resource "null_resource" "helm" {
  depends_on = [
    "tls_self_signed_cert.ca",
    "tls_private_key.helm",
    "tls_locally_signed_cert.helm",
    "null_resource.kubernetes-roles"
  ]
  triggers {
    cert = "${tls_locally_signed_cert.helm.cert_pem}"
  }
  provisioner "local-exec" {
    environment {
      CA_PEM = "${tls_self_signed_cert.ca.cert_pem}"
      SERVER_KEY = "${tls_private_key.helm.private_key_pem}"
      SERVER_CRT = "${tls_locally_signed_cert.helm.cert_pem}"
    }
    working_dir = "/tmp"
    command = "${path.module}/install-tiller.sh"
  }
  provisioner "local-exec" {
    command = "kubectl delete -n kube-system secret -l app=helm"
    when = "destroy"
  }
  provisioner "local-exec" {
    command = "kubectl delete -n kube-system all -l app=helm"
    when = "destroy"
  }
}
