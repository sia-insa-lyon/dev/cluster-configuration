resource "tls_private_key" "user" {
  algorithm   = "RSA"
  rsa_bits = 2048
}

resource "tls_cert_request" "user" {
  key_algorithm   = "RSA"
  private_key_pem = "${tls_private_key.user.private_key_pem}"

  subject {
    common_name  = "helm-user"
    organization = "${var.organization}"
    organizational_unit = "${var.organizational_unit}"
    country = "${var.country}"
  }
}

resource "tls_locally_signed_cert" "user" {
  cert_request_pem   = "${tls_cert_request.user.cert_request_pem}"
  ca_key_algorithm   = "RSA"
  ca_private_key_pem = "${tls_private_key.ca.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca.cert_pem}"

  validity_period_hours = 219000 # This is 25 years

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "client_auth"
  ]
}

resource "local_file" "helm_cert" {
  filename = "${pathexpand("~/.helm/cert.pem")}"
  sensitive_content = "${tls_locally_signed_cert.user.cert_pem}"
}

resource "local_file" "helm_key" {
  filename = "${pathexpand("~/.helm/key.pem")}"
  sensitive_content = "${tls_private_key.user.private_key_pem}"
}

resource "local_file" "helm_ca" {
  filename = "${pathexpand("~/.helm/ca.pem")}"
  sensitive_content = "${tls_self_signed_cert.ca.cert_pem}"
}
