locals {
  ingress_namespace = "ingress"
  le_env = "production"
}

resource "null_resource" "certmanager_crd_setup" {
  provisioner "local-exec" {
    command = "kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml"
  }
  provisioner "local-exec" {
    command = "kubectl delete -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml"
    when = "destroy"
  }
}

resource "helm_release" "certmanager" {

  depends_on = [
    "null_resource.certmanager_crd_setup"
  ]

  chart = "stable/cert-manager"
  name = "web-pki"
  namespace = "${local.ingress_namespace}"

  values = [
    <<YAML
ingressShim:
  defaultIssuerName: letsencrypt-${local.le_env}
  defaultIssuerKind: ClusterIssuer
  defaultACMEChallengeType: dns01
  defaultACMEDNS01ChallengeProvider: cloudflare
webhook:
  enabled: false
YAML
  ]

}

resource "null_resource" "lets_encrypt_setup" {
  depends_on = [
    "null_resource.certmanager_crd_setup"
  ]
  triggers {
    le_env="${local.le_env}"
  }
  provisioner "local-exec" {
    command = "kubectl apply -n ${local.ingress_namespace} -f ${path.module}/letsencrypt-${local.le_env}.yaml"
  }
}

resource "kubernetes_secret" "cloudflare" {
  depends_on = [
    "helm_release.haproxy"
  ]
  "metadata" {
    name = "cloudflare"
    namespace = "${local.ingress_namespace}"
  }
  data {
    api = "${var.cloudflare_apiKey}"
  }
}

resource "helm_release" "external_dns" {
  chart = "stable/external-dns"
  name = "dns"
  namespace = "${local.ingress_namespace}"

  values = [
    <<YAML
cloudflare:
  apiKey: "${var.cloudflare_apiKey}"
  email: "${var.cloudflare_email}"
  proxied: false
domainFilters:
  - "asso-insa-lyon.fr"
provider: cloudflare
logLevel: debug
rbac:
  create: true
  serviceAccountName: "cloudflare"
YAML
  ]
}

resource "helm_release" "haproxy" {
  chart = "incubator/haproxy-ingress"
  name = "haproxy"
  namespace = "${local.ingress_namespace}"

  timeout = "900"
  depends_on = [
    "helm_repository.incubator"
  ]

  values = [
    <<YAML
controller:
  config:
    use-proxy-protocol: "true"
  extraArgs:
    publish-service: ${local.ingress_namespace}/haproxy-haproxy-ingress-controller
  service:
    annotations:
      service.beta.kubernetes.io/ovh-loadbalancer-proxy-protocol: "v1"
  stats:
    enabled: true
  metrics:
    enabled: true
defaultBackend:
  image:
    repository: gcr.io/google_containers/echoserver
    tag: 1.10
YAML
  ]
}

