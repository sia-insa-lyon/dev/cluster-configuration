terraform {
  backend "remote" {
    organization = "sia-insa-lyon"
    "workspaces" {
      prefix = "ingress-"
    }
  }
}

provider "dns" {}
provider "kubernetes" {}
provider "helm" {
  enable_tls = true
  namespace = "kube-system"
  insecure = true
  install_tiller = false
}

resource "helm_repository" "incubator" {
  name = "incubator"
  url = "https://kubernetes-charts-incubator.storage.googleapis.com/"
}
