variable "cloudflare_apiKey" {}
variable "cloudflare_email" {}
variable "domain" {
  default = "asso-insa-lyon.fr"
}
