terraform {
  backend "remote" {
    organization = "sia-insa-lyon"
    "workspaces" {
      prefix = "keycloak-"
    }
  }
}

resource "random_string" "keycloak_database_password" {
  length = 16
  special = false
}

variable "deployment" {
  default = "keycloak"
}

locals {
  namespace = "${var.deployment}"
  database = "db-${var.deployment}"
  database_name = "keycloak"
  bucket = "keycloak-backup-${terraform.workspace}"
}

resource "kubernetes_namespace" "keycloak" {
  "metadata" {
    name = "${local.namespace}"
    labels {
      env = "production"
      security = "high"
      rgpd = "sensitive"
    }
  }
}

resource "kubernetes_config_map" "init" {
  depends_on = ["kubernetes_namespace.keycloak"]
  "metadata" {
    name = "keycloak-init-script"
    namespace = "${local.namespace}"
  }
  data {
    init.sql = <<SQL
CREATE DATABASE ${local.database_name}
SQL
  }
}

module "backup-bucket" {
  source = "../minio-bucket"
  bucketName = "keycloak-backup-${terraform.workspace}"
}

module "database" {
  source = "../postgres"
  name = "${local.database}"
  namespace = "${local.namespace}"
  minioBucket = "${local.bucket}"
  minioAccessToken = "${module.backup-bucket.access_token}"
  minioSecretToken = "${module.backup-bucket.secret_token}"
  replicas = "2"
  size = "3Gi"
  # See on https://kubedb.com/docs/0.12.0/guides/postgres/initialization/replay_from_minio/ to restore in case of disaster
  init = <<YAML
scriptSource:
  configMap:
    name: keycloak-init-script
YAML
  depends_on = [
    "${kubernetes_namespace.keycloak.metadata.uid}", "${kubernetes_config_map.init.metadata.uid}"
  ]
}

variable "domain" {
  default = "sso.asso-insa-lyon.fr"
}

resource "helm_release" "keycloak" {
  chart = "stable/keycloak"
  name = "keycloak"
  namespace = "${local.namespace}"

  depends_on = [
    "module.database", "module.backup-bucket"
  ]

  values = [
    <<FOO
keycloak:
  username: "admin"
  password: ${random_string.keycloak_database_password.result}
  extraEnv: |
    - name: PROXY_ADDRESS_FORWARDING
      value: "true"
  ingress:
    enabled: true
    annotations:
      kubernetes.io/tls-acme: "true"
    hosts:
      - ${var.domain}
    tls:
      - hosts:
        - ${var.domain}
        secretName: keycloak-cert
  persistence:
    dbVendor: postgres
    dbHost: ${module.database.host}
    dbName: ${local.database_name}
    dbUser: ${module.database.username}
    dbPassword: ${module.database.password}
FOO
  ]

}

output "admin_default_keycloak" {
  value = "${random_string.keycloak_database_password.result}"
}
