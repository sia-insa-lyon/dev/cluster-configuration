# KubeDB Operator

KubeDB helps us to manage databases inside our clusters and must be seen as critical.

To setup it, we use the helm deployment recomanded [here](https://kubedb.com/docs/0.12.0/setup/install/)

## Bug Notice (11/05/2019)
We are having some issue with the official Helm image because it creates the operator before the CRD. To avoid that, we found a patch but need you to run first a:
```
terraform init
terraform workspace select production
terraform init

# Step 1: Install kubedb operator chart
terraform apply -target=helm_release.kubedb

# Step 2: wait until crds are registered
kubectl get crds -l app=kubedb -w

# Step 3: Install KubeDB catalog of database versions
terraform apply
```

Follow the bug correction here: https://github.com/kubedb/project/issues/504
