terraform {
  backend "remote" {
    organization = "sia-insa-lyon"
    "workspaces" {
      prefix = "kubedb-"
    }
  }
}

provider "helm" {
  enable_tls = true
  namespace = "kube-system"
  insecure = true
  install_tiller = false
}

data "helm_repository" "appscode" {
  name = "appscode"
  url  = "https://charts.appscode.com/stable/"
}

resource "helm_release" "kubedb" {
  chart = "kubedb"
  repository = "${data.helm_repository.appscode.name}"
  name = "kubedb-operator"
  version = "0.12.0"
  namespace = "kube-system"

  wait = false

  set {
    name = "rbac.create"
    value = "true"
  }
  set {
    name = "serviceAccount.create"
    value = "true"
  }
  set {
    name = "serviceAccount.name"
    value = "kubedb"
  }
}

resource "helm_release" "kubedb-catalog" {
  chart = "kubedb-catalog"
  repository = "${data.helm_repository.appscode.name}"
  name = "kubedb-catalog"
  version = "0.12.0"
  namespace = "kube-system"

  depends_on = [
    "helm_release.kubedb"
  ]
}
