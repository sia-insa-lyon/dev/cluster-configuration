variable "bucketName" {}
variable "region" {
  default = "ovh-gra5"
}
locals {
  policy = "${var.bucketName}-owner-policy"
}
resource "random_string" "access_token" {
  length = 20
  special = false
}
resource "random_string" "secret_token" {
  length = 20
  special = false
}
data "template_file" "policy" {
  vars {
    bucket = "${var.bucketName}"
  }
  template = "${file("${path.module}/policy.json")}"
}
variable "minioHost" {
  default = "sia"
}
resource "null_resource" "policy" {
  triggers {
    host = "${var.minioHost}"
    name = "${var.bucketName}"
    policy = "${local.policy}"
  }
  provisioner "local-exec" {
    environment {
      POLICY = "${data.template_file.policy.rendered}"
    }
    command = "echo \"$POLICY\" > /tmp/policy.json; mc admin policy add ${var.minioHost} ${local.policy} /tmp/policy.json; rm /tmp/policy.json"
  }
  provisioner "local-exec" {
    command = "mc admin policy remove ${var.minioHost} ${local.policy}"
    when = "destroy"
  }
}

resource "null_resource" "user" {
  depends_on = [
    "null_resource.policy"
  ]
  triggers {
    host = "${var.minioHost}"
    name = "${var.bucketName}"
    policy = "${local.policy}"
    identifier = "${sha512(md5("${var.bucketName}${random_string.access_token.result}${random_string.secret_token.result}"))}"
  }
  provisioner "local-exec" {
    environment {
      ACCESS_TOKEN = "${random_string.access_token.result}"
      SECRET_TOKEN = "${random_string.secret_token.result}"
    }
    command = "mc admin user add ${var.minioHost} $ACCESS_TOKEN $SECRET_TOKEN ${local.policy}"
  }
  provisioner "local-exec" {
    environment {
      ACCESS_TOKEN = "${random_string.access_token.result}"
    }
    command = "mc admin user remove ${var.minioHost} $ACCESS_TOKEN"
    when = "destroy"
  }
}

resource "null_resource" "bucket" {
  triggers {
    host = "${var.minioHost}"
    name = "${var.bucketName}"
    region = "${var.region}"
  }
  provisioner "local-exec" {
    command = "mc mb ${var.minioHost}/${var.bucketName} --region=${var.region}"
  }
  provisioner "local-exec" {
    command = "mc rb ${var.minioHost}/${var.bucketName}"
    when = "destroy"
  }
}
output "access_token" {
  value = "${random_string.access_token.result}"
  sensitive = true
}
output "secret_token" {
  value = "${random_string.secret_token.result}"
  sensitive = true
}
