#!/bin/bash

echo "This file must not be run as a script, open it to look to the commands"
exit 1

mc admin policy add sia keycloak-backup keycloak-backup-policy.json
mc admin user add sia $USER_ACCESS_TOKEN $USER_SECRET_TOKEN keycloak-backup
mc mb sia/keycloak-backup --region=ovh-gra5
