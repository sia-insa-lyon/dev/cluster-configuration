# S3 Storage

S3 is a common used protocol to store data as Object. The issue for a small 
organisazion (and more over for a non-profit), the storage costs are quite too
high.

To avoid that, the solution is to host by yourself an S3 service. But how? reimplement all the S3 protocol is a costly work and we can make a lot of mistake. Hopefully, we do not have to do it. We will deploy [minio](https://min.io/).

## Minio Deployment

We deploy Minio through a Helm chart. In this repository a Terraform definition help you to manage Minio inside your cluster.

You will have to create the `terraform.tfvars` files with configuration settings then run the Terraform plan as:
```
terraform init
terraform apply
```
