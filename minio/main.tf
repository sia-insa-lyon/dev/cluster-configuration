terraform {
  backend "remote" {
    organization = "sia-insa-lyon"
    "workspaces" {
      prefix = "cluster-minio-"
    }
  }
}

provider "helm" {
  enable_tls = true
  namespace = "kube-system"
  insecure = true
  install_tiller = false
}

