locals {
  minio_namespace = "minio"
  minio_secret = "minio-admin"
}

resource "kubernetes_namespace" "minio" {
  "metadata" {
    name = "${local.minio_namespace}"
  }
}

variable "domain" {
  default = "s3.asso-insa-lyon.fr"
}

resource "random_string" "access_key" {
  length = 18
  special = false
}

output "minio_access_key" {
  value = "${random_string.access_key.result}"
  sensitive = true
}

resource "random_string" "secret_key" {
  length = 32
  special = false
}

output "minio_secret_key" {
  value = "${random_string.secret_key.result}"
  sensitive = true
}

resource "kubernetes_secret" "minio" {
  depends_on = ["kubernetes_namespace.minio"]
  metadata {
    name = "${local.minio_secret}"
    namespace = "${local.minio_namespace}"
  }
  data {
    accesskey = "${random_string.access_key.result}"
    secretkey = "${random_string.secret_key.result}"
  }
}

resource "kubernetes_persistent_volume_claim" "minio_storage" {
  depends_on = ["kubernetes_namespace.minio"]
  metadata {
    name = "${local.minio_namespace}"
    namespace = "${local.minio_namespace}"
  }
  lifecycle {
    prevent_destroy = true
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    "resources" {
      requests {
        storage = "75Gi"
      }
    }
    storage_class_name = "cinder-classic"
  }
}

variable "region" {
  default = "ovh-gra5"
}
resource "helm_release" "minio" {
  chart = "stable/minio"
  name = "minio"
  namespace = "${local.minio_namespace}"

  depends_on = [
    "kubernetes_namespace.minio",
    "kubernetes_secret.minio",
    "kubernetes_persistent_volume_claim.minio_storage"
  ]

  values = [ <<YAML
ingress:
  enabled: true
  hosts:
    - ${var.domain}
  annotations:
    kubernetes.io/tls-acme: "true"
  tls:
    - hosts:
      - ${var.domain}
      - "*.${var.domain}"
      secretName: s3-cert
existingSecret: ${local.minio_secret}
persistence:
  existingClaim: ${local.minio_namespace}
defaultBucket:
  name: public
environment:
  MINIO_DOMAIN: "${var.domain}"
  MINIO_STORAGE_CLASS_STANDARD: "EC:4"
  MINIO_STORAGE_CLASS_RRS: "EC:1"

YAML
  ]

}
