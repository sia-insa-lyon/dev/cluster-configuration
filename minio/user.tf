resource "local_file" "minio_setup" {
  filename = "${pathexpand("~/.sia-admin/sensitive/minio.sh")}"
  sensitive_content = <<BASH
#!/bin/bash
mc config host add sia https://${var.domain} ${random_string.access_key.result} ${random_string.secret_key.result}
BASH
  provisioner "local-exec" {
    working_dir = "${pathexpand("~/.sia-admin")}"
    command = "./encrypt minio.sh"
  }
  provisioner "local-exec" {
    working_dir = "${path.module}"
    command = "bash ${pathexpand("~/.sia-admin/sensitive/minio.sh")}"
  }
}
