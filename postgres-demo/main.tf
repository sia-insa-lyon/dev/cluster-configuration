module "backup-bucket" {
  source = "../minio-bucket"
  bucketName = "test-postgres"
}

resource "kubernetes_namespace" "test" {
  "metadata" {
    name = "test-db"
  }
}

module "my-database" {
  source = "../postgres"
  namespace = "test-db"
  name = "test-db"
  replicas = "3"
  minioBucket = "test-postgres"
  minioAccessToken = "${module.backup-bucket.access_token}"
  minioSecretToken = "${module.backup-bucket.secret_token}"
  depends_on = ["${kubernetes_namespace.test.id}"]
}
