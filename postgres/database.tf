variable "namespace" {}
variable "name" {}
variable "depends_on" {
  default = []
  type = "list"
}
variable "version" {
  default = "10.2-v4"
  description = "KubeDB Postgres version (see https://kubedb.com/docs/0.12.0/guides/postgres/quickstart/quickstart/#find-available-postgresversion)"
}

variable "storageClass" {
  default = "cinder-high-speed"
}

variable "size" {
  default = "1Gi"
}

variable "replicas" {
  default = "1"
}

variable minioBucket {
  type = "string"
  default = "backup"
}
variable minio {
  type = "string"
  default = "https://s3.asso-insa-lyon.fr"
}
variable minioAccessToken {
  type = "string"
}
variable minioSecretToken {
  type = "string"
}
resource "kubernetes_secret" "archiverSecret" {
  "metadata" {
    name = "${var.name}-s3"
    namespace = "${var.namespace}"
  }
  data {
    AWS_ACCESS_KEY_ID = "${var.minioAccessToken}"
    AWS_SECRET_ACCESS_KEY = "${var.minioSecretToken}"
    AWS_ENDPOINT = "${var.minio}"
    AWS_REGION = "ovh-gra5"
  }
}

variable "init" {
  default = <<YAML
{}
YAML
}

data "template_file" "database-tmpl" {
  template = "${file("${path.module}/database.yml")}"
  vars = {
    namespace = "${var.namespace}"
    name = "${var.name}"
    version = "${var.version}"
    storageClass = "${var.storageClass}"
    size = "${var.size}"
    replicas = "${var.replicas}"
    archiverSecret = "${var.name}-s3"
    archiverBucket = "${var.minioBucket}"
    archiverEndpoint = "${var.minio}"
    init = "${indent(4, var.init)}"
  }
}

resource "random_string" "username" {
  length = 10
  special = false
}

output "host" {
  value = "${var.name}.${var.namespace}.svc.cluster.local"
}

output "username" {
  value = "${random_string.username.result}"
}

resource "random_string" "password" {
  length = 20
  special = false
}

output "password" {
  value = "${random_string.password.result}"
  sensitive = true
}

resource "kubernetes_secret" "credentials" {
  "metadata" {
    namespace = "${var.namespace}"
    name = "${var.name}-credentials"
  }
  data {
    POSTGRES_USER = "${random_string.username.result}"
    POSTGRES_PASSWORD = "${random_string.password.result}"
  }
}

resource "null_resource" "apply" {
  triggers {
    definition = "${data.template_file.database-tmpl.rendered}"
  }
  provisioner "local-exec" {
    environment {
      DEFINITION = "${data.template_file.database-tmpl.rendered}"
      NAME = "${var.name}"
      NAMESPACE = "${var.namespace}"
    }
    working_dir = "${path.module}"
    command = "timeout 300s ./kubectl.sh apply"
  }
  depends_on = ["kubernetes_secret.archiverSecret", "kubernetes_secret.credentials", "null_resource.cleaner"]
}

resource "null_resource" "cleaner" {
  provisioner "local-exec" {
    environment {
      DEFINITION = "${data.template_file.database-tmpl.rendered}"
    }
    working_dir = "${path.module}"
    command = "./kubectl.sh delete --ignore-not-found"
    when = "destroy"
  }
}
