#!/usr/bin/env bash

echo "${DEFINITION}" | kubectl $@ -f -
if [[ $1 == "apply" ]]; then
    sleep 10
    while [[ $(kubectl get postgres/$NAME -n $NAMESPACE -o jsonpath={.status.phase}) != "Running" ]]; do
        echo "Waiting for DB to be ready..."
        sleep 10
    done
fi
