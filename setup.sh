#!/usr/bin/env bash


echo "SIA INSA Lyon - Cluster Configuration\n";
echo -n "This script must only be run by SIA Administrators, do you confirm ? [Y/n] "
read continue
if [[ ${continue} == "n" ]]; then
    echo "Ok, good bye"
    return 0;
fi

echo -n "Looking for sia-admin storage... "
if [[ -d ~/.sia-admin/ ]]; then
    SIA_ADMIN=~/.sia-admin/
    echo "FOUND";
else
    echo "FAILED";
    return 1;
fi

${SIA_ADMIN}/decrypt

_ensure_to_have(){
    if [[ ! -f ${SIA_ADMIN}/sensitive/$1 ]]; then
        echo "Unable to find the sensitive data $1"
        return 1;
    fi
}

_ensure_to_have ovh.sh
_ensure_to_have intermediate_ca.pem
_ensure_to_have intermediate_ca-key.pem
_ensure_to_have openstack.sh

. ${SIA_ADMIN}/sensitive/ovh.sh
. ${SIA_ADMIN}/sensitive/openstack.sh
